import React from 'react';
import ReactDOM from 'react-dom';
import background from './app/images/background_form.jpg';
import App from './app/App';
import registerServiceWorker from './registerServiceWorker';
import {injectGlobal} from 'styled-components';

injectGlobal`
  body, html, #root {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    width: 100%;
    height: 100%;
  }
  body {
    background-image: url(${background});
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: no-repeat;
  }
  
  #root {
    display: flex;
    justify-content: center;
  }

`;

ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();
