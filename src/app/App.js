/* @flow */
import React, {Component} from 'react';
import logo from './images/logo.svg';
import FormBlock from "./components/FormBlock/FormBlock";
import {Header, Image, Intro, Wrapper} from "./App.styled";

type AppProps = {};

class App extends Component<AppProps> {

  render() {
    return (
        <Wrapper>
          <Header>
            <Image src={logo} alt="logo"/>
            <h1>Welcome to Email Example Form</h1>
          </Header>
          <Intro>
            <FormBlock/>
          </Intro>
        </Wrapper>
    );
  }
}

export default App;
