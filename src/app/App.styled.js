import styled, {keyframes} from 'styled-components';

export const LogoSpin = keyframes`
      from { transform: rotate(0deg); }
      to { transform: rotate(360deg); }
    `;

export const Wrapper = styled.section`
      position: relative;
      height: 100%;
      background: linear-gradient(to bottom, rgba(146, 135, 187, 0.8) 0%, rgba(0,0,0,0.6) 100%);
      transition: opacity 0.1s, transform 0.3s cubic-bezier(0.17, -0.65, 0.665, 1.25);
      transform: scale(1);    
      width: 300px;
      padding: 25px;
      box-sizing: border-box;
    `;

export const Header = styled.section`
      height: 150px;
      padding: 20px;
      color: white;
      text-align: center;
    `;

export const Intro = styled.section`
      font-size: 10px
    `;

export const Image = styled.img`
      animation: ${LogoSpin} infinite 20s linear;
      height: 80px;
    `;
