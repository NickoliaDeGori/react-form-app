/* @flow */
import React from 'react';
import {ErrorTitle, Icon, Input, InputWrapper} from "./InputBlock.styled";

type InputBlockProps = {
  name: string,
  placeholder: string,
  onChange: (SyntheticInputEvent<EventTarget>) => void,
  value: string,
  error: string,
};

const InputBlock = ({name, placeholder, onChange, value, error}: InputBlockProps) => {

  return (
      <InputWrapper isError={!!error}>
        <Icon isError={!!error} name={name}/>
        <Input
            name={name}
            type={name}
            id={name}
            placeholder={placeholder}
            value={value}
            autocomplete="off"
            onChange={onChange}
        />
        {!!error &&
        <ErrorTitle id={`${name}_error`}>{error}</ErrorTitle>
        }
      </InputWrapper>
  )
};

export default InputBlock;
