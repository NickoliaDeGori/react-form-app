/* @flow */
import React, {Component} from 'react';
import {testEmail, testPassword} from "../../../utils/inputs";
import InputBlock from "../InputBlock/InputBlock";
import {Button, Form} from "./FormBlock.styled";

type FormBlockProps = {};

type FormBlockState = {
  emailError: string,
  passwordError: string,
  email: string,
  password: string,
  emailTaps: number,
  passwordTaps: number,
  isButtonDisabled: boolean,
  isAuth: boolean,
};

class App extends Component<FormBlockProps, FormBlockState> {

  state = {
    emailError: "",
    passwordError: "",
    email: "",
    password: "",
    emailTaps: 0,
    passwordTaps: 0,
    isButtonDisabled: true,
    isAuth: false,
  };

  validateInput = (name: string, value: string): string => {
    if (this.state[`${name}Taps`] < 2) {
      return '';
    }

    if (this.state[`${name}Taps`] >= 2 && value.length === 0) {
      return 'Заполните поле';
    }

    switch (name) {
      case 'password':
        return !testPassword(value) ? 'Пароль должен быть длиинной не менее 4-х символов' : '';
      case 'email':
        return !testEmail(value) ? 'Неправильный формат адреса электронной почты' : '';
      default:
        return ''
    }
  };

  onClickLogin = (event: SyntheticInputEvent<EventTarget>) => {
    event.preventDefault();
    if (!this.isInputsValid) {
      this.setState({
        emailError: this.validateInput('email', this.state.email),
        passwordError: this.validateInput('password', this.state.password),
      });
    } else {
      this.setState({
        isAuth: true,
      })
    }
  };

  onChangeText = (name: string, val: SyntheticInputEvent<EventTarget>) => {
    const newState = {
      [name]: val.target.value,
      [`${name}Taps`]: this.state[`${name}Taps`] + 1,
      [`${name}Error`]: this.validateInput(name, val.target.value),
    };
    const isInputsValid = this.isInputsValid({
      ...this.state,
      ...newState,
    });
    this.setState({
      ...newState,
      isButtonDisabled: !isInputsValid,
    });
  };

  isInputsValid = (newState: FormBlockState) => {
    return !newState.emailError &&
        !newState.passwordError &&
        (newState.emailTaps > 2 || newState.email.length > 2) &&
        (newState.passwordTaps > 2 || newState.password.length > 2)
  };

  render() {
    return (
        <Form onSubmit={this.onClickLogin}>
          <InputBlock
              name="email"
              placeholder="E-mail"
              onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("email", val)}
              value={this.state.email}
              error={this.state.emailError}
          />
          <InputBlock
              name="password"
              placeholder="Пароль"
              onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("password", val)}
              value={this.state.password}
              error={this.state.passwordError}
          />
          <Button
              id="submit_button"
              type='submit'
              disabled={this.state.isButtonDisabled}
          >
            SignIn
          </Button>
        </Form>
    );
  }
}

export default App;
