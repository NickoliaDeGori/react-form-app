import React from 'react';
import {mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import FormBlock from '../src/app/components/FormBlock/FormBlock';

configure({ adapter: new Adapter() });

describe('Form', () => {
  const wrapper = mount(<FormBlock />);

  it('Should button is disabled after render', () => {
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have email-error and button is disabled after enter "exam" to email input', () => {
    wrapper.instance().onChangeText("email", {target: {value: "e"}});
    wrapper.instance().onChangeText("email", {target: {value: "ex"}});
    wrapper.instance().onChangeText("email", {target: {value: "exa"}});
    wrapper.instance().onChangeText("email", {target: {value: "exam"}});
    wrapper.update();
    expect(wrapper.state().emailError).toEqual('Неправильный формат адреса электронной почты');
    expect(wrapper.find("#email_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });


  it('Should state have email-error and button is disabled after remove all words in email input', () => {
    wrapper.instance().onChangeText("email", {target: {value: ""}});
    expect(wrapper.state().emailError).toEqual('Заполните поле');
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have password-error and button is disabled after enter "123" to password input', () => {
    wrapper.instance().onChangeText("password", {target: {value: "1"}});
    wrapper.instance().onChangeText("password", {target: {value: "12"}});
    wrapper.instance().onChangeText("password", {target: {value: "123"}});
    wrapper.update();
    expect(wrapper.state().passwordError).toEqual('Пароль должен быть длиинной не менее 4-х символов');
    expect(wrapper.find("#password_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have not email-error and button is disabled after enter "example@gmail.ru" to email input', () => {
    wrapper.instance().onChangeText("email", {target: {value: "e"}});
    wrapper.instance().onChangeText("email", {target: {value: "ex"}});
    wrapper.instance().onChangeText("email", {target: {value: "exa"}});
    wrapper.instance().onChangeText("email", {target: {value: "exam"}});
    wrapper.instance().onChangeText("email", {target: {value: "examp"}});
    wrapper.instance().onChangeText("email", {target: {value: "exampl"}});
    wrapper.instance().onChangeText("email", {target: {value: "example"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@g"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gm"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gma"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmai"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmail"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmail."}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmail.c"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmail.co"}});
    wrapper.instance().onChangeText("email", {target: {value: "example@gmail.com"}});
    wrapper.update();
    expect(wrapper.state().emailError).toEqual('');
    expect(wrapper.find("#email_error").exists()).toBeFalsy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have not password-error and button is  not disabled after enter "12345" to password input', () => {
    wrapper.instance().onChangeText("password", {target: {value: "1234"}});
    wrapper.instance().onChangeText("password", {target: {value: "12345"}});
    wrapper.update();
    expect(wrapper.state().passwordError).toEqual('');
    expect(wrapper.find("#password_error").exists()).toBeFalsy();
    expect(wrapper.state().isButtonDisabled).toBeFalsy();
  });

  it('Clicked to button', () => {
    wrapper.instance().onClickLogin({preventDefault: ()=>{}});
    wrapper.update();
    expect(wrapper.state().isAuth).toBeTruthy();
  });
});
