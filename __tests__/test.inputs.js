import {testEmail, testPassword} from "../src/utils/inputs"

test('example@gmail.com email is correct', () => {
  expect(testEmail("example@gmail.com")).toBeTruthy();
});

test('example123123.123.1421#!@gmail.com email is correct', () => {
  expect(testEmail("example123123.123.1421#!@gmail.com")).toBeTruthy();
});

test('example123123.123.1421#!@gmail email is incorrect', () => {
  expect(testEmail("example123123.123.1421#!@gmail")).toBeFalsy();
});

test('example123123@23.1421#!@gmail email is incorrect', () => {
  expect(testEmail("example123123@23.1421#!@gmail")).toBeFalsy();
});

test('example123123@23.1421#!gmail email is incorrect', () => {
  expect(testEmail("example123123@23.1421#!gmail")).toBeFalsy();
});

test('rwtycf24 password is correct', () => {
  expect(testPassword("rwtycf24")).toBeTruthy();
});

test('rw password is correct', () => {
  expect(testPassword("rw")).toBeFalsy();
});
